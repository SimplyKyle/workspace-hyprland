use anyhow::{Context, Result};
use clap::Parser;
use hyprland::data::{Monitors, Workspaces};
use hyprland::event_listener::EventListenerMutable;
use hyprland::prelude::*;
use hyprland::shared::WorkspaceType::{Regular, Special};
use lazy_static::lazy_static;

use std::process::Command;
use std::sync::{Arc, Mutex};

lazy_static! {
    static ref ARGS: Args = Args::parse();
}

fn eww_widget_output(active: Option<i32>, workspaces: &mut Vec<i32>) {
    // make `-99` the last
    if workspaces.iter().any(|x| *x == -99) {
        workspaces.rotate_left(1);
    }
    let mut output = String::new();
    output.push('[');
    for (x_pos, x) in workspaces.iter().enumerate() {
        if x_pos == workspaces.len() - 1 {
            if *x != -99 {
                output.push_str(&x.to_string())
            } else {
                output.push_str("Special");
            }
        } else {
            if *x != -99 {
                output.push_str(&format!("{x}, "));
            } else {
                output.push_str("Special, ");
            }
        }
    }
    output.push(']');
    Command::new("eww")
        .arg("update")
        .arg(format!("{}={}", ARGS.main_update_name, output))
        .output()
        .unwrap();
    if active == Some(-99) {
        Command::new("eww")
            .arg("update")
            .arg(format!("{}=Special", ARGS.active_update_name))
            .output()
            .unwrap();
    } else if active.is_some() {
        Command::new("eww")
            .arg("update")
            .arg(format!(
                "{}={}",
                ARGS.active_update_name,
                active.context("Unreachable").unwrap()
            ))
            .output()
            .unwrap();
    } else {
        dbg!("fucking none");
    }
}

fn make_workspace(monitor_name: &str) -> Result<(Option<i32>, Vec<i32>)> {
    let all_monitors = Monitors::get()?;
    let all_workspaces = Workspaces::get()?;

    let active_workspace = all_monitors
        .filter(|x| &x.name == monitor_name && x.focused)
        .map(|x| x.active_workspace)
        .next();
    let active = match active_workspace {
        Some(x) => Some(x.id),
        None => None,
    };
    println!("{active:?}");
    let mut occupied_workspace = all_workspaces
        .filter(|x| &x.monitor == monitor_name)
        .map(|x| x.id)
        .collect::<Vec<i32>>();

    occupied_workspace.sort_unstable();

    Ok((active, occupied_workspace))
}

#[derive(Parser, Debug)]
struct Args {
    monitor: String,
    #[clap(long = "cheat")]
    cheat: bool,
    #[clap(long = "active_update_name")]
    active_update_name: String,
    #[clap(long = "main_update_name")]
    main_update_name: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    let cheat = &ARGS.cheat;
    let monitor_name = &ARGS.monitor;

    // inital output
    let (active_workspace, mut occupied_workspaces) = make_workspace(&monitor_name)?;
    let occupied_workspaces_ref = Arc::new(Mutex::new(occupied_workspaces.clone()));
    eww_widget_output(active_workspace, &mut occupied_workspaces);

    let mut event_listener = EventListenerMutable::new();
    let closures_clone1 = Arc::clone(&occupied_workspaces_ref);
    let closures_clone2 = Arc::clone(&occupied_workspaces_ref);
    let closuers_clone3 = Arc::clone(&occupied_workspaces_ref);
    let closuers_clone4 = Arc::clone(&occupied_workspaces_ref);
    let closuers_clone5 = Arc::clone(&occupied_workspaces_ref);

    if !cheat {
        event_listener.add_workspace_change_handler(move |workspace, _| {
            let changed_to = match &workspace {
                Regular(x) => x,
                Special(_) => "-99",
            };
            dbg!(&changed_to);
            let refworkspace = &mut *closuers_clone3.lock().unwrap();
            let id = changed_to
                .parse::<i32>()
                .context("Can't parse name to id, plz don't use named workspace")
                .unwrap();
            eww_widget_output(Some(id), refworkspace);
        });

        event_listener.add_workspace_added_handler(move |workspace, state| {
            if monitor_name == &state.active_monitor {
                let added = match &workspace {
                    Regular(x) => x,
                    Special(_) => "-99",
                };
                let id = added
                    .parse::<i32>()
                    .context("Can't parse name to id, plz don't use named workspace")
                    .unwrap();
                let refworkspace = &mut *closures_clone1.lock().unwrap();
                refworkspace.push(id);
                refworkspace.sort_unstable();
                dbg!(&refworkspace);
                eww_widget_output(Some(id), refworkspace);
            }
        });

        event_listener.add_workspace_destroy_handler(move |workspace, state| {
            if monitor_name == &state.active_monitor {
                let active = match &state.active_workspace {
                    Regular(x) => x,
                    Special(_) => "-99",
                };
                let removed = match &workspace {
                    Regular(x) => x,
                    Special(_) => "-99",
                };
                let refworkspace = &mut *closures_clone2.lock().unwrap();
                let removed_id = removed
                    .parse::<i32>()
                    .context("Can't parse name to id, plz don't use named workspace")
                    .unwrap();
                let active_id = active
                    .parse::<i32>()
                    .context("Can't parse name to id, plz don't use named workspace")
                    .unwrap();
                let pos = refworkspace.iter().position(|x| *x == removed_id);
                if let Some(x) = pos {
                    refworkspace.swap_remove(x);
                    refworkspace.sort_unstable();
                }
                dbg!(&refworkspace);
                eww_widget_output(Some(active_id), refworkspace);
            }
        });

        event_listener.add_active_monitor_change_handler(move |mon, _| {
            let active_workspace = mon.1;
            let active = match &active_workspace {
                Regular(x) => x,
                Special(_) => "-99",
            };
            let refworkspace = &mut *closuers_clone4.lock().unwrap();
            let id = active
                .parse::<i32>()
                .context("Can't parse name to id, plz don't use named workspace")
                .unwrap();
            eww_widget_output(Some(id), refworkspace);
        });

        event_listener.add_workspace_moved_handler(move |mon, _state| {
            let moved_to_monitor = mon.0;
            // mon.1 is workspace that's going to be moved over to
            let workspace_named = match &mon.1 {
                Regular(x) => x,
                Special(_) => "-99",
            };
            let refworkspace = &mut *closuers_clone5.lock().unwrap();
            let id = workspace_named
                .parse::<i32>()
                .context("Can't parse name to id, plz don't use named workspace")
                .unwrap();
            if monitor_name == &moved_to_monitor {
                refworkspace.push(id);
                refworkspace.sort_unstable();
                dbg!(&refworkspace);
                eww_widget_output(Some(id), refworkspace);
            } else {
                refworkspace.remove(refworkspace.iter().position(|&x| x == id).unwrap());
                eww_widget_output(Some(id), refworkspace);
            }
        });
    } else {
        event_listener.add_workspace_change_handler(|_, _| {
            let (active_workspace, mut occupied_workspaces) = make_workspace(monitor_name).unwrap();
            eww_widget_output(active_workspace, &mut occupied_workspaces);
        });
        event_listener.add_workspace_added_handler(|_, _| {
            let (active_workspace, mut occupied_workspaces) = make_workspace(monitor_name).unwrap();
            eww_widget_output(active_workspace, &mut occupied_workspaces);
        });
        event_listener.add_workspace_destroy_handler(|_, _| {
            let (active_workspace, mut occupied_workspaces) = make_workspace(monitor_name).unwrap();
            eww_widget_output(active_workspace, &mut occupied_workspaces);
        });
        event_listener.add_active_monitor_change_handler(move |mon, _| {
            let active = match &mon.1 {
                Regular(x) => x,
                Special(_) => "-99",
            };
            let (_, mut occupied_workspaces) = make_workspace(monitor_name).unwrap();
            eww_widget_output(Some(active.parse().unwrap()), &mut occupied_workspaces);
        });
        event_listener.add_workspace_moved_handler(move |_, _| {
            let (active_workspace, mut occupied_workspaces) = make_workspace(monitor_name).unwrap();
            eww_widget_output(active_workspace, &mut occupied_workspaces);
        });
    }

    event_listener.start_listener_async().await?;

    println!("Event listener started");

    Ok(())
}
